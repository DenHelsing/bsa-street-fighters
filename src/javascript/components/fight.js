import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const keysPressed = {};
    const firstFighterHealthBar = document.querySelector('#left-fighter-indicator.arena___health-bar');
    const secondFighterHealthBar = document.querySelector('#right-fighter-indicator.arena___health-bar');
    const CRIT_TIMEOUT = 10000;

    let critAllowedForFirst = true;
    let critAllowedForSecond = true;

    const dealDamageToHero = (attacker, defender, defenderHealthBar, typeOfStrike = 'usual') => {
      const damage = typeOfStrike === 'usual' ? getDamage(attacker, defender) : 2 * attacker.attack;
      if (defenderHealthBar.style.width === '') {
        defenderHealthBar.style.width = `${100 - (damage / defender.health) * 100}%`;
      } else {
        const currentHealth = parseFloat(defenderHealthBar.style.width);
        const newHealth = currentHealth - (damage / defender.health) * 100;
        defenderHealthBar.style.width = `${newHealth < 0 ? 0 : newHealth}%`;
      }
    };

    document.addEventListener('keyup', (e) => {
      delete keysPressed[e.code];
    });
    document.addEventListener('keydown', (e) => {
      keysPressed[e.code] = true;

      if (e.code === controls.PlayerOneAttack) {
        if (!keysPressed[controls.PlayerTwoBlock] && !keysPressed[controls.PlayerOneBlock]) {
          dealDamageToHero(firstFighter, secondFighter, secondFighterHealthBar);
        } else {
          console.log('attack 1 unsuccesfull');
        }
      }
      if (e.code === controls.PlayerTwoAttack) {
        if (!keysPressed[controls.PlayerTwoBlock] && !keysPressed[controls.PlayerOneBlock]) {
          dealDamageToHero(secondFighter, firstFighter, firstFighterHealthBar);
        } else {
          console.log('attack 2 unsuccesfull');
        }
      }

      if (
        critAllowedForFirst &&
        keysPressed[controls.PlayerOneCriticalHitCombination[0]] &&
        keysPressed[controls.PlayerOneCriticalHitCombination[1]] &&
        keysPressed[controls.PlayerOneCriticalHitCombination[2]]
      ) {
        critAllowedForFirst = false;
        setTimeout(() => {
          critAllowedForFirst = true;
        }, CRIT_TIMEOUT);
        dealDamageToHero(firstFighter, secondFighter, secondFighterHealthBar, 'critical');
      }
      if (
        critAllowedForSecond &&
        keysPressed[controls.PlayerTwoCriticalHitCombination[0]] &&
        keysPressed[controls.PlayerTwoCriticalHitCombination[1]] &&
        keysPressed[controls.PlayerTwoCriticalHitCombination[2]]
      ) {
        critAllowedForSecond = false;
        setTimeout(() => {
          critAllowedForSecond = true;
        }, CRIT_TIMEOUT);
        dealDamageToHero(secondFighter, firstFighter, firstFighterHealthBar, 'critical');
      }

      console.log(firstFighterHealthBar.style.width);
      console.log(secondFighterHealthBar.style.width);
      if (firstFighterHealthBar.style.width === '0%') {
        resolve(secondFighter);
      }
      if (secondFighterHealthBar.style.width === '0%') {
        resolve(firstFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
