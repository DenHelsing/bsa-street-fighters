import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter !== undefined) {
    try {
      console.log(fighter);
      const fighterImage = createElement({
        tagName: 'img',
        className: 'fighter-preview___image',
        attributes: { src: fighter.source },
      });
      const fighterProperties = createElement({ tagName: 'div', className: 'fighter-preview___properties' });
      const fighterHealth = createElement({ tagName: 'span' });
      fighterHealth.appendChild(document.createTextNode(`Health: ${fighter.health}`));
      const fighterDefense = createElement({ tagName: 'span' });
      fighterDefense.appendChild(document.createTextNode(`Defense: ${fighter.defense}`));
      const fighterAttack = createElement({ tagName: 'span' });
      fighterAttack.appendChild(document.createTextNode(`Attack: ${fighter.attack}`));
      fighterProperties.appendChild(fighterHealth);
      fighterProperties.appendChild(fighterDefense);
      fighterProperties.appendChild(fighterAttack);
      const fighterName = createElement({ tagName: 'p', className: 'fighter-preview___name' });
      fighterName.appendChild(document.createTextNode(fighter.name));
      fighterElement.appendChild(fighterName);
      fighterElement.appendChild(fighterImage);
      fighterElement.appendChild(fighterProperties);
    } catch (error) {
      console.log(error);
    }
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
