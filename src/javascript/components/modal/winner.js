import { showModal } from './modal';

export function showWinnerModal(fighter) {
  console.log(fighter);
  const modalWindow = document.createElement('div');
  modalWindow.style.height = '15px';
  modalWindow.style.background = 'white';

  showModal({ title: `${fighter.name} WON! Congrats!`, bodyElement: modalWindow });
}
